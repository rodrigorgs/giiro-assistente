var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

map.on('click', clicouNoMapa);
carregaMarcadores();

//////////////

function carregaMarcadores() {
	$.get('/markers', function(data) {
		list = JSON.parse(data);
		for (var i = 0; i < list.length; i++) {
			var coords = list[i];
			adicionaMarcador(new L.LatLng(coords[0], coords[1]));
		}
	});
}

function adicionaMarcador(latlng) {
	var marker = L.marker(latlng, {draggable: true});
	marker.on('popupopen', abrePopup)
	marker.addTo(map)
		.bindPopup("<a class='apagar' href='#'>Apagar</a>");
}

function clicouNoMapa(e) {
	$.ajax({
	    url: '/markers',
		type: 'POST',
		contentType: 'application/json',
	    dataType: 'json',
		data: JSON.stringify([e.latlng.lat, e.latlng.lng])
	})
	adicionaMarcador(e.latlng);
}

function abrePopup() {
	var marker = this;
	$('.apagar').click(function() {
		map.removeLayer(marker);
	});
}