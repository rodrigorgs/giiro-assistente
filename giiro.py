from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
import json
import sys

app = Flask(__name__, static_path='/static')

marcadores = [[51.51291, -0.09716], [51.4972, -0.09905]]

@app.route("/")
def exibePaginaInicial():
	return "<a href='static/index.html'>GIIRO</a>"

@app.route("/markers")
def listaMarcadores():
	return json.dumps(marcadores)

@app.route("/markers", methods=['POST'])
def adicionaMarcador():
	global marcadores
	coords = json.loads(request.data.decode("utf-8"))
	marcadores.append(coords)
	return '', 200

if __name__ == "__main__":
	app.debug = True
	app.run()