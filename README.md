# GIIRO - Seleção para Assistente

Este projeto é um protótipo de aplicação web que usa mapas através da biblioteca [Leaflet](http://leafletjs.com/). O protótipo está incompleto; cabe a você implementar algumas funcionalidades que ficaram faltando.

## Primeiros passos

Baixe este projeto para sua máquina usando o Git.

O projeto foi testado com o Python 3.4. As bibliotecas necessárias para rodar a aplicação estão especificadas no arquivo `requirements.txt`.

Para rodar o servidor web com a aplicação, execute o arquivo `giiro.py`.

## Funcionalidades

- A aplicação exibe um mapa e permite ao usuário inserir, mover e remover marcadores, que representam pontos de interesse.
- Para remover um marcador, clique sobre o mesmo e então clique no link "Apagar".
- Se a página for atualizada, a quantidade de marcadores é preservada.

## Tarefa

As funcionalidades a seguir são muito importantes, mas não foram implementadas ainda. Sua tarefa é implementá-las.

- O mapa deve mostrar por padrão a cidade de Salvador, em uma escala que permita ver as principais vias da cidade.
- Se o usuário mover um marcador e atualizar a página, o marcador deve ficar na última posição para o qual foi movido.
- Os marcadores devem ser persistidos em disco, de forma que a sua posição não se perca quando o servidor web é reinicializado.
    - Se for usar um banco de dados relacional, use o SQLite.
- O mapa deve ocupar todo o espaço disponível no navegador (altura e largura).

## Orientações

- Não divulgue seu código-fonte para ninguém além de mim.
- Se usar algum trecho de código da web (ex.: StackOverflow), cite a fonte nos comentários do seu programa.
- Programe como se você estivesse trabalhando em um projeto real, atentando para a qualidade.

## Entrega

Envie seu código de uma das seguintes duas formas:

- Preferencialmente, crie um repositório privado no GitLab ou no BitBucket e compartilhe comigo (usuário: `rodrigorgs`).
- Se não for possível, envie o código em um arquivo .zip para o meu e-mail.

Envie um e-mail para mim informando os dias e horários preferenciais para uma entrevista.

## Prazo

Você tem até o dia 16/12/2015 (quarta-feira) para enviar o seu programa.
